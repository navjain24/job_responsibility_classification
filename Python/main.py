from sentenceTagger import *
from common import *
from sentenceExtractor import *
from sentenceEmbedder import *
from masterDataPreparer import *


extractSentences(BRONZE_DATA, SILVER_DATA)
embedSentences(SILVER_DATA, GOLD_DATA)
dict = prepareMasterEmbeddings(MASTER_DATA)
tagSentences(GOLD_DATA, dict, TAGGED_DATA)
