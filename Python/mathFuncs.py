import numpy as np
from numpy.linalg import norm

def calculateCosineSimilarity(arrayA, arrayB):
    cosine = np.dot(arrayA,arrayB)/(norm(arrayA)*norm(arrayA))
    return cosine