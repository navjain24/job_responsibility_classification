from pathlib import Path
from common import *
from nltk.tokenize import sent_tokenize
from embedder import *


# Read folders;
#   for each folder found:
#       Read the responsibilities.txt
#           for each line in the text, create embedding
def prepareMasterEmbeddings(rootFolder):
    print('Processing root folder: %s' % rootFolder)

    dict = {}
    for child in Path(rootFolder).iterdir():
        if child.is_file() == False:
            jobRole = child.name
            sourceFileFolder = f"{rootFolder}/{jobRole}"
            print(f"Found directory: {sourceFileFolder}")

            for p in Path(sourceFileFolder).glob('*.txt'):
                sourceFullFilePath = f"{sourceFileFolder}/{p.name}"
                dict[jobRole] = embedSentencesFromSingleMasterJob(jobRole, sourceFullFilePath)
    return dict


def embedSentencesFromSingleMasterJob(jobRole, sourceFullFilePath):
    print(f"Processing {jobRole}:{sourceFullFilePath}")
    tempDict = {}
        
    with open(sourceFullFilePath) as f:
        s = f.read()

        sentences = sent_tokenize(s)
        i = 0
        for sentence in sentences:
            i += 1
            embedding = createEmbedding(sentence)
            # print(f"[{jobRole}] Master embedding - {i}:{embedding}")
            tempDict[sentence] = embedding
    return tempDict    

            