from pathlib import Path
from mathFuncs import *
from common import *
from embedder import *
import numpy as np
import json


def tagSentences(sourceRootFolder, dict, destinationRootFolder):
    print('Processing root folder: %s' % sourceRootFolder)

    for child in Path(sourceRootFolder).iterdir():
        if child.is_file() == False:
            jobRole = child.name
            if jobRole in dict.keys():
                sourceFileFolder = f"{sourceRootFolder}/{jobRole}"
                print(f"Found directory: {sourceFileFolder}")

                for p in Path(sourceFileFolder).glob('*.embedding'):
                    sourceFullFilePath = f"{sourceFileFolder}/{p.name}"
                    tagSentencesFromSingleJob(jobRole, dict[jobRole], sourceFullFilePath, destinationRootFolder)


def tagSentencesFromSingleJob(jobRole, subDict, sourceFullFilePath, destinationRootFolder):
    print(f"Processing {jobRole}:{sourceFullFilePath}")

    # Create a new file for each sentence and store the same
    # The content hash of the text is the file name.
    makeDirIfNeeded(destinationRootFolder)

    destinationFolder = f"{destinationRootFolder}/{jobRole}"
    makeDirIfNeeded(destinationFolder)

    with open(sourceFullFilePath) as f:
        json_object = json.load(f)
        sentence = json_object[SENTENCE]
        embedding = json_object[EMBEDDING]
        contentHash = createHash(sentence)
        
        tagged_sentence = {}
        tagged_sentence[SENTENCE] = sentence

        bestScore = 0.0
        bestSentence = ''
        foundMatch = False
        for masterSentence, masterEmbedding in subDict.items():
            comparisonScore = calculateCosineSimilarity(embedding, masterEmbedding)
            print(f"Got cosine as:{comparisonScore} ")
            if comparisonScore > 0.5:
                print(f"Got threshold cosine as:{comparisonScore}")
                if comparisonScore > bestScore:
                   bestScore = comparisonScore
                   bestSentence = masterSentence
                   foundMatch = True

        if foundMatch == True:
            # store best matched master sentence as tag
            tagged_sentence[TAG] = bestSentence
            
            fileName = f"{contentHash}.tag"
            destinationFilePath = f"{destinationFolder}/{fileName}"
            
            with open(destinationFilePath, 'w') as f:
                f.write(json.dumps(tagged_sentence, indent=4))
                print(f"Tagged the sentence from: {sourceFullFilePath} to file: {destinationFilePath}")
        