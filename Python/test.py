from mathFuncs import *
from embedder import *

s1 = "My name is Arnav J"
s2 = "Arnav J is not my city"

def compare(s1, s2):
    embedding1 = createEmbedding(s1)
    print(f"e1: {embedding1}")

    embedding2 = createEmbedding(s2)
    print(f"e2: {embedding2}")

    similarityScore = calculateCosineSimilarity(embedding1, embedding2)
    print(f"score: {similarityScore}")

    if similarityScore > 0.85:
        print("Sentence match!")
    else:
        print("NOT matched")

compare(s1, s2)
    


