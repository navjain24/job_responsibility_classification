from pathlib import Path
from common import *
from embedder import *
import numpy as np
import json 



def embedSentences(sourceRootFolder, destinationRootFolder):
    print('Processing root folder: %s' % sourceRootFolder)

    for child in Path(sourceRootFolder).iterdir():
        if child.is_file() == False:
            jobRole = child.name
            sourceFileFolder = f"{sourceRootFolder}/{jobRole}"
            print(f"Found directory: {sourceFileFolder}")

            for p in Path(sourceFileFolder).glob('*.sentence'):
                sourceFullFilePath = f"{sourceFileFolder}/{p.name}"
                embedSentencesFromSingleJob(jobRole, sourceFullFilePath, destinationRootFolder)


def embedSentencesFromSingleJob(jobRole, sourceFullFilePath, destinationRootFolder):
    print(f"Processing {jobRole}:{sourceFullFilePath}")

    # Create a new file for each sentence and store the same
    # The content hash of the text is the file name.
    makeDirIfNeeded(destinationRootFolder)

    destinationFolder = f"{destinationRootFolder}/{jobRole}"
    makeDirIfNeeded(destinationFolder)

    with open(sourceFullFilePath) as f:
        s = f.read()
        embedding = createEmbedding(s)
        contentHash = createHash(s)
        # Create JSON object to store sentence and corresponding embedding
        tempDict = {}
        tempDict[SENTENCE] = s
        #tempDict["embedding"] = np.array2string(embedding)
        tempDict[EMBEDDING] = embedding.tolist()

        json_object = json.dumps(tempDict, indent=4)
        print(json_object)

        #write json object to a file
        fileName = f"{contentHash}.embedding"
        destinationFilePath = f"{destinationFolder}/{fileName}"

        with open(destinationFilePath, 'w') as f:
            f.write(json_object)
            print(f"Embedded the sentence from: {sourceFullFilePath} to file: {destinationFilePath}")
     