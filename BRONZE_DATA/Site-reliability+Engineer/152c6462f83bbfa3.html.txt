
 
  
   Are you ready to power the World's connections?
  
  
  
    If you don’t think you meet all of the criteria below but are still interested in the job, please apply. Nobody checks every box - we’re looking for candidates that are particularly strong in a few areas, and have some interest and capabilities in others.
  
  
  
    About the role:
  
  
  
    Design, develop, and maintain microservices that power Kong Konnect, the Service Connectivity Platform on top of Azure. Working closely with Product Management and teams across Engineering, you will develop software that has a direct impact on our customers' business and Kong's success.
  
  
  
    Why should you want to work at Kong?
  
  
  
    Market Opportunity - We are on a quest to build a $10b+ software company over the next few years and need YOUR help!
  
  
    Why APIs Matter? APIs have been enabling innovation for decades!
  
  
    Strong VC team, Series D, strong year over year revenue growth!
  
  
    Technical Leadership - We are recognized as the leader in innovation in the connectivity space.
  
  
    Marco, our CTO/co-founder - “We are the Cisco of L4 and L7” - CUBE Conversation, March 2021
  
  
    We are the leading innovator in the connectivity space!
  
  
    Amazing Team & Culture - Come be a "Konger" and find out what we mean.
  
  
    Great Place to Work Certified in 2020 & 2021
  
  
    Kong employees exemplify our culture at our 2022 Sales Kickoff
  
  
    Building Great Products - Learn why the world's largest companies love our tech!
  
  
    Kong Named a Leader in the 2022 Gartner Magic Quadrant
  
  
    Over 250m+ downloads of our open source API gateway! Over 60k+ stars on Github between Kong API and Kong Insomnia!
  
  
 
  
   What you'll do:
   
    
      Partner across KONG, combine Software and Systems knowledge to engineer high-volume distributed systems in a reliable, scalable, and fault-tolerant manner.
      Responsible for infrastructure design, build, integration, and maintain CI/CD pipelines for integrating changes and deploying to production in progressively tested environments.
      Collaborating closely with engineering teams on building and enhancing tooling and automation solutions for faster resolution of issues impacting SLO's and averting incidents altogether when possible.
      Communicate on a deeply technical level and be the single point of contact for interfacing with KONG Infrastructure for handling service escalations and driving the issues to resolution.
      Develop and drive real time observability solutions that provide visibility into system health.
      Serve on team responsible for maintaining service 24 x 7 to meet client expectations of accessible services.
      Represent KONG in the technology community.
      And any additional tasks required by manager.
    
   
  
  
 
  
   What you'll bring:
   
    
      Bachelor's Degree in Computer Science, Information Technology, or related field AND 5+ years technical experience in software engineering, network engineering, or systems administration .
      Strong experience using and managing Azure (preferred) or other cloud-native infrastructure with Infrastructure as Code and configuration management.
      Fluency in a modern programming language (Golang, Python, Ruby, etc.)
      Experience with continuous/rapid release engineering (CI/CD). "Infrastructure as Code" configuration management systems such as Terraform, Chef, Puppet or Ansible.
      Experience building and administering alerting and monitoring systems for API services.
      Strong knowledge of Linux/Unix systems.
      Strong skills in network services such as DNS, TLS/SSL, HTTP.
      Experience working in a 24/7/365 service environment.
      Design, implement, manage and orchestrate Kubernetes container clusters.
      Service Reliability/Operational experience running large scale high-performance systems Internet services
      Advanced knowledge of systems applications, server architecture, operating platforms, Cloud technologies, internet and web applications
    
   
  
  
 
  
   Kong has different base pay ranges for different work locations globally, which allows us to pay employees competitively and consistently in different geographic markets. Compensation varies depending on a wide array of factors, including but not limited to specific candidate location, role, skill set and level of experience. Certain roles are eligible for additional rewards including sales incentives depending on the terms of the applicable plan and role. Benefits may vary depending on location. US based employees are typically offered access to healthcare benefits, a 401(k) plan, short and long term disability benefits, basic life and AD&D insurance, among others. The typical base pay range for this role in the USD is $225,000 - 275,000 USD.
  
  
  
    What is a Konger?
  
  
  
    We are a group of makers, thinkers, and doers focused on helping today’s developers build tomorrow’s technology. Our teams work on the bleeding edge of API innovation to provide our users with a central nervous system for data and services.
  
  
  
    We put design at the heart of everything we do, and we’re relentlessly focused on creating beautiful experiences for our customers. That’s why technology companies, major banks, e-commerce innovators, and government agencies put Kong in front of their most important web applications.
  
  
  
    We believe in the power of Open Source and everything it stands for. That’s why developers around the world enthusiastically contribute on top of our open-source platform.
  
  
  
    We are passionate about solving challenges that will fundamentally shape the future of technology, and we’re looking for the right people to join us on our mission. If you believe in taking ownership of your work, making an impact, and having fun along the way, we would love to talk to you.
  
  
  
    Kong Core Values:
  
  
  
    Be Inclusive. We work together from anywhere to achieve our common goals. Our differences make us stronger.
  
  
  
    Be Authentic. We are genuine, principled and confident without arrogance. Show respect and kindness, especially in tough moments.
  
  
  
    Be Relentlessly Resourceful. We work with purpose, obsession and grit. It takes muscle to do hard things and doing hard things build muscle.
  
  
  
    Be Customer Obsessed. We care. Customers are everything, we put them at the center of everything you do. We are all empowered to make an impact.
  
  
  
    Be Curious. We value ideas over hierarchy. Never accept the status quo. We make bold bets, fail, and learn everyday. There is always a way.
  
  
  
    Be an Owner. We are drivers not passengers and own the quality and outcomes of our work.
  
  
  
    About Kong:
  
  
  
    Kong is THE cloud native API platform with the fastest, most adopted API gateway in the world (over 300m downloads!). Loved by developers and trusted with enterprises’ most critical traffic volumes, Kong helps startups and Fortune 500 companies build with confidence – allowing them to bring solutions to market faster with API and service connectivity that scales easily and securely.
  
  
  
    83% of web traffic today is API calls! APIs are the connective tissue of the cloud and the underlying technology that allows software to talk and interact with one another. Therefore, we believe that APIs act as the nervous system of the cloud. Our audacious mission is to build the nervous system that will safely and reliably connect all of humankind!
  
  
  
    For more information about Kong, please visit konghq.com or follow @thekonginc on Twitter.
  
  
  
    We are an equal opportunity employer. All qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender identity, national origin, disability or veteran status.
  
 
