Who We Are?
Anker Innovations is a global leader in smart charging technologies and a developer of consumer products for the home,car, and mobile life Founded in 2011 by Steven Yang, Anker quickly established itself as an innovator and market leader in intelligent charging solutions.
Anker Innovations is committed to shaping the consumer electronics brand in the global marketplace, bringing innovative, technologically savvy leading products to consumers around the world.The products have sold to more than 100 countries and regions around the world, with more than 100 million users.
Since established, Anker Innovations has continued to lead the way in terms of revenue scale and growth rate.In 2020, Anker Innovations sold more than 50 million pieces products worldwide with a revenue of 1433 million USD, up 40.54% over the same period of time as compared to the previous year.
Anker Innovations has successfully built Anker, a world-renowned high-end innovative charging brand, and launched intelligent hardware brands such as Eufy, Nebula, and Soundcore to further explore smart charging, smart voice, smart home and other fields, bringing leading products with technological charm to the market.
At Anker, 1010 out of 2144 employees are engaged in research and development，and the proportion of R&D personnel up to 50%.
We are engaged in the world's leading research and development of charging, audio, home appliances, automotive, projection and other product technologies, with more than 1478 intellectual property rights, to ensure that our products continue to be popular.
For more info，pls visit www.anker-in.com
Job summary
Join us on the US SR team at Anker, We are hiring for experienced Site Reliability Engineers to
build native data cloud center of highly reliable, largely scaled, and based on micro-service infrastructures in the US.
What You’ll Do?

 Cooperate with the SREs of China and maintain AWS cloud resources, including deployment, troubleshooting, software update, data query, etc.
 Improve operational processes, such as deployments and upgrades, to make them as standardized as possible.
 Improve the alarm monitoring, maintain the operation of business and emergency response.
 Responsible for maintenance of localized data and routine drill of operation.
 Maintain services by measuring and monitoring availability, latency and overall system health.
 Support programmers, testers and other team members to deal with technical issues.

Basic Qulifications

 Bachelor's degree majoring in CS, or related field with at least 1 year of related work experience.
 Experience in SRE of large-scale systems deployment with high reliability and scalability.
 In-depth understanding of virtualization technology, familiar with docker, Docker-compose, Kubernetes, etc.
 Familiar with programming language of Python and Golang, Familiar with Shell script.
 Proficient in using monitor system, such as Zabbix, Grafana or Prometheus.
 Understanding CI/CD and have practical experience in DevOps.
 Familiar with common log collection schemes (e.g. collection and display of EKS business logs and container logs).

Preferred Qulifications

 Good understanding of IT security
 Distributed systems knowledge
 Analytical problem-solving skills
 Ability to speak Chinese is preferred.

Job Type: Full-time
Pay: $83,373.07 - $125,653.41 per year
Benefits:

 401(k)
 401(k) matching
 Dental insurance
 Employee discount
 Flexible schedule
 Health insurance
 Paid time off
 Vision insurance

Schedule:

 8 hour shift

Experience:

 Azure: 1 year (Preferred)
 AWS: 1 year (Preferred)
 Kubernetes: 1 year (Preferred)

Work Location: In person