
 Overview: 
 
   Responsible for the security, reliability and efficiency of production systems. Telecommuting permitted from anywhere within the U.S.
  Responsibilities: 
 
   Responsible for the security, reliability and efficiency of production systems. Serve on team responsible for maintaining service 24 x 7 to meet client expectations of accessible data services. Analyze data in support of Tier1 client support tickets. Develop, maintain, and optimize the software development and deployment environments. Responsible for infrastructure, build, integration, and software deployment process. Deploy and manage both Iaas and Pass services in development and production. Develop and drive real time observability solutions that provide visibility into system health. Provide technical guidance and educate team members and coworkers on development and operations best practices. Brainstorm for new ideas and ways to improvement development delivery. Manage CI and CD tools with team. Develop and implement security measures related to the development process. Deploy and monitor intrusion detection and vulnerability scanning system. Design and implement build, deployment, and configuration management. Build and test automation tools for infrastructure provisioning. Telecommuting permitted from anywhere within the U.S.
  Qualifications: 
 
   REQUIRED QUALIFICATIONS: 
 
 
  Master’s or foreign equivalent degree in Computer Science, Engineering, Information Systems, or a related field 
  3 years of experience in each of the following: software engineering or site reliability engineering: load balancing and automation; infrastructure automation tools Terraform, Docker, and/or Jenkins; AWS, including EC2 and ELB; CI/CD; SQL database design and development; and automation using scripting. All experience may be gained concurrently.
 
 
 
   TERMS OF EMPLOYMENT: 40+ hours per week; $152,145-$152,145 per year. 
 
 
  Base wage may be based on various factors, such as geographic location, candidate experience and qualifications, as well as market and business considerations. This role is eligible for an annual bonus based on individual and company performance, depending on the terms of the applicable plan and the employee’s role. Avalara’s benefits for eligible employees includes company benefits such as medical, dental, and vision coverage, life, AD&D, and disability insurance, a 401(k) retirement plan, 17 days of paid time off annually, 12 paid holidays, paid parental leave, an employee assistance program, and subsidized transportation options for commuters. All benefits are subject to eligibility requirements and Avalara reserves the right to modify or change these benefits programs at any time, with or without notice, unless otherwise required by law.
 
 
  TO APPLY: Apply online at https://careers.avalara.com/north-america under Requisition ID 11091. About Avalara: 
 
   About Avalara:
   We’re building cloud-based tax compliance solutions to handle every transaction in the world. Imagine every transaction you make — every tank of gas, cup of coffee, or pair of sneakers, every movie ticket, meal kit, or streamed song, every sensor-to-sensor ping. Nearly every time you make a purchase, physical or digital, there’s an accompanying unique and nuanced tax compliance calculation. The logic behind calculating taxes — the rules, rates, and boundaries is a global, layered, three-dimensional mess of complexity, with compliance dictated by governments and applied by every business, every day.
 
 
 
   Avalara works with businesses of all sizes, all over the world — from corner stores to gigantic global retailers — to calculate tax accurately and automatically, at speeds measured in milliseconds. That’s a massive technical challenge, in terms of scale, reliability, and complexity, and we do it better than anyone. That’s why we’re growing fast. Headquartered in Seattle, Avalara has offices across the U.S. and around the world, in Brazil, Canada, India, U.K, Belgium and across Europe.
 
 
 
   Equal Opportunities: 
   Avalara is an Equal Opportunity Employer. All qualified candidates will receive consideration for employment without regard to race, colour, creed, religion, age, gender, national orientation, disability, sexual orientation, US Veteran status, or any other factor protected by law.
 
