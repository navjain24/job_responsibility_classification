
 
  
   
    Summary 
    
     
      Posted: Jun 22, 2023
      
     
      Weekly Hours: 
      40
      
     
      Role Number:
      200487512
      
    
    
     Do you love engineering and running systems and infrastructure that will delight millions of customers? Imagine what you could do here. At Apple, new ideas have a way of becoming extraordinary products, services, and customer experiences very quickly! Bring passion and dedication to your job, and there’s no telling what you could accomplish. The iWork SRE team is looking for a Service Reliability Engineer (SRE) to design, build tools and support our large-scale content system for iCloud. The best candidates will have proven software development skills and strong Linux / Systems expertise, understand SRE, and know what it will take to run services at Apple scale with high operational precision. We play a critical role in the day-to-day operations of services relied upon across Apple while partnering with engineering teams to ensure we, and they are successful!
     
   
  
 
 
  
   
    
     
      Key Qualifications 
     
     
      
       
        3+ years in a Site Reliability Engineering, DevOps, or Infrastructure focused role 
        Experience supporting internet-facing production services and distributed systems 
        Ability to implement and coordinate telemetry using monitoring and observability tools such as Splunk, Grafana, and Prometheus 
        Strong verbal and written communication skills 
        Coding experience using a high-level programming language like Java, Golang, or Python 
        Automation advocate - you truly believe in removing operational load via software 
        A strong sense of ownership. At the same time, you’re a great teammate who communicates clearly and transparently - Self-motivated, inquisitive, and always looking to learn more 
        Experience managing, scaling, and troubleshooting Java applications 
        Familiarity with cloud infrastructure concepts (zones, regions, VPCs, etc.) 
        An understanding of a variety of software service deployment packaging, strategies, and tooling 
        Working understanding of common authentication schemes, certificates, and securely managing secrets 
        Capable of designing and implementing automated configuration management processes for repeatable and consistent service deployment 
       
      
     
    
   
  
  
   
    
     
      Description 
     
     
      
       Our team is collaborative; we work closely with partner teams to deliver the best results for Apple. We strive to balance the best solution with the need to get things done for each engineering challenge we face. Good ideas are heard, and results are rewarded. As an SRE at Apple, you will: • Operate, monitor, and triage all aspects of our production and non-production environments. • Pioneer and implement the next-generation telemetry system for iWork. • Prepare alert handling procedures, runbooks, and collaborate with the off-shore SRE teams. • Automate deployment and orchestration of services into the cloud environment as well as other routine processes. • Actively participate in capacity planning, scale testing, and disaster recovery exercises. • Interact with and support partner teams, including engineering, QA, and program management. • Cultivate and maintain relationships with internal and external third-party vendors.
       
     
    
   
  
  
   
    
     
      Education & Experience 
     
     
      
       Bachelor's Degree in Computer Science, an engineering-related field, or equivalent related experience. Advanced Degree preferred.
       
     
    
   
  
  
   
    
     
      Additional Requirements 
     
     
      
       
        
         Prior experience as an SRE, software engineer, or system administrator
         
        
         Experience in system automation technology, such as Puppet or Ansible
         
        
         Experience in Kubernetes
         
       
      
     
    
   
  
  
   
    
     
      Pay & Benefits 
     
     
      
       
        At Apple, base pay is one part of our total compensation package and is determined within a range. This provides the opportunity to progress as you grow and develop within a role. The base pay range for this role is between $161,700 and $284,900, and your base pay will depend on your skills, qualifications, experience, and location.  Apple employees also have the opportunity to become an Apple shareholder through participation in Apple’s discretionary employee stock programs. Apple employees are eligible for discretionary restricted stock unit awards, and can purchase Apple stock at a discount if voluntarily participating in Apple’s Employee Stock Purchase Plan. You’ll also receive benefits including: Comprehensive medical and dental coverage, retirement benefits, a range of discounted products and free services, and for formal education related to advancing your career at Apple, reimbursement for certain educational expenses — including tuition. Additionally, this role might be eligible for discretionary bonuses or commission payments as well as relocation. Learn more about Apple Benefits.  Note: Apple benefit, compensation and employee stock programs are subject to eligibility requirements and other terms of the applicable plan or program.
       
      
     
    
   
  
 
