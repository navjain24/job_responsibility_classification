
 Description 
 THE ROLE 
 Service Engineers work in partnership with developers, testers, and program managers, (typically early in the development process) to design, operate, grow, and improve large-scale, computer-based services offered to IS, non-IS and clinical caregivers. Their technical skills are on par with their Development, Test, Operations, and Program Management counterparts. This role requires someone to be both a leader and a coach as we bring in a cutting-edge service management mindset and skill set to the organization. It requires a leader to deal with a high degree of ambiguity and drive clear strategy and implementation plans, while bringing the business, fellow IS (Information Management) organizations and the team of Service Engineers all to success. The scope is both broad, across a business unit and deep, in terms of technical, process and continuous improvement required to run and maintain an enterprise wide IS service. 
 ESSENTIAL FUNCTIONS 
 
  Influences the division-wide caregiver and business themes for the service portfolio and leads their group to define a service vision. 
  Leads the group to design services that deliver the expected value to the caregiver and business objectives to Providence. Ensures that the service vision and designs that can be iterated and are well-positioned for continued delivery. 
  Anticipates industry and market trends, advises business leaders on new solutions, and consistently defines successful solution/services that help Providence succeed. 
  Guides the creation of conceptual models for service delivery and its direction that incorporate market, technology, and business trends. 
  Advises business leaders on needed capabilities and technologies, and how to build or acquire them to drive continued future success. 
  Uses a cross-service perspective to lead the planning and design of a service delivery solution that defines tools, hardware, processes, role assignments, dependencies, and documentation, resulting in a complete solution that delivers manageable, supportable, performant, resilient services that meet KPIs. 
  Leads the strategy for the outbound messaging to enable the adoption and support for the solution/service portfolio scenarios. 
  Leads work streams across professions, such as clinical, marketing, finance, and business development to develop the overall business strategy for the solution/service portfolio, and is responsible for a successful business model, value proposition, go-to-market strategy, messaging, support structure, and readiness. 
  Defines the division’s requirements and strategy to ensure Providence stakeholder readiness and the appropriate internal tools, systems, and processes needed to resolve issues and manage information flow, to and from caregiver/partners. 
  Guides the development of the division’s overall content strategy to deliver the appropriate tools, documentation, and communication to drive adoption and enable caregivers to enjoy the full value of Providence solutions. 
  Is the most visible representative and the leading spokesperson for the solution/service portfolio in the ecosystem and is responsible for the overall ecosystem strategy for the solution/service portfolio. 
  Prioritizes work, across the solution/service portfolio, and removes barriers to the organization’s agility to enable teams to shift priorities quickly without losing solutions. 
  Promotes a culture of quality and ensures that the organization’s standards are consistent with the overall solution/service objectives. 
  Makes difficult decisions to meet quality and service delivery and/or shipping goals. 
  Optimizes or transforms the engineering lifecycle for the solution/service portfolio or company to achieve significant engineering system. 
  Promotes a culture of quality and ensures that the standards of the Operations and Engineering teams (including PM, Development, and Test) are consistent with the overall service objectives. Leads Service Engineers in the design of process or technology solutions that identify and resolve service issues, prior to solution release, and enable an on-time release and measurable improvements against KPIs. 
  Provides leadership within the business by developing innovative methods for measuring the caregiver experience and uses this data to identify and drive service and business improvements. Analyzes operational cost data, identifies cost saving efficiencies, and influences the business to adopt these efficiencies across multiple service teams across the division. 
  Builds a diverse team with capabilities needed to achieve current and future business objectives. Balances the development of existing employees while bringing in new external and internal talent. Creates an inclusive work environment where every employee can effectively engage and wants to be part of the team. Provides ongoing feedback that helps direct reports improve their performance. Leverages resources to help employees develop skills and support their career interests. 
  Effectively communicates business strategy and goals and how they align to the team’s work. Plans the team’s work to achieve goals and respond effectively to changing priorities, maintaining team energy and results. Holds the team and individuals accountable for results and recognize appropriately. Partners and collaborates with other teams on related deliverables, and effectively leverages others in relevant work streams. Facilitates an environment of inclusion that leverages diverse perspectives and talent, to better represent and understand our global caregivers. 
  Through behaviors and actions, sets an example and represent Providence values and culture. 
 
 QUALIFICATIONS 
 
  Bachelor's Degree in Computer Engineering, Computer Science, Mathematics, Engineering; or equivalent educ/experience 
  Master's Degree in Computer Engineering, Computer Science, Mathematics, Engineering; or equivalent educ/experience (preferred) 
  10 or more years of related experience, more preferred 
  10 or more years of experience leading technical teams in a complex and fast-moving environment 
  Strong experience with leveraging the power of data and metrics to drive behavior, process and priority decisions across the team and IS (Information Services) 
  Experience with Azure Dev Ops platform management 
  Demonstrated ability to lead software development and automation functions, developing strategies that will increase efficiency and effectiveness across all functions 
  History of creating and implementing software development plans to support business needs, leveraging technology to improve processes 
  Significant experience in the development, coding, and maintenance of systems for automation and integration across an organization to streamline processes and increase efficiency 
  Experienced in management of a team of software development and automation professionals, providing guidance, mentorship, and support to ensure team success 
  Collaboration with other departments and to identify software needs and develop solutions that improve their workflows to drive consistent methods and process that will demonstrate PSHJ is a leader in the Healthcare technology space 
  Strong ability to analyze software development and automation results and make strategic adjustments as needed to meet goals 
  Evaluate and implement automation technologies to improve efficiency and effectiveness 
  Proven ability to lead the development of a product roadmap and ensure that development timelines and budgets are met 
  Management of relationships with vendors and software providers, negotiating contracts and ensuring service level agreements are met 
  Strong knowledge in development, service management, site reliability, and having the ability to work hands on and to teach and grow others in the knowledge and skills 
  Strong ability to work as a key member of a tight knit executive leadership team to create a high performing team that will drive necessary a change in a consistent way throughout the organization 
  Strong knowledge in leveraging the power of data and metrics to drive behavior, process and priority decisions across the team and IS (Information Services) 
  Ability to demonstrate and act as a key Service Engineering leader across the IS team to drive consistent methods and process that will demonstrate PSHJ is a leader in the Healthcare technology space 
  Strong ability to perform work independently with minimal supervision, to lead and grow a technical team, growing and hiring the next set of leaders, both managerially and technically 
  Good ability to prioritize responsibilities and to organize workload to ensure that timeframes are met and the work is successfully completely within deadlines 
  Excellent verbal and written communication skills with the ability to create technical presentations/documents. Basic project management. 
 
 About Providence 
 At Providence, our strength lies in Our Promise of “Know me, care for me, ease my way.” Working at our family of organizations means that regardless of your role, we’ll walk alongside you in your career, supporting you so you can support others. We provide best-in-class benefits and we foster an inclusive workplace where diversity is valued, and everyone is essential, heard and respected. Together, our 120,000 caregivers (all employees) serve in over 50 hospitals, over 1,000 clinics and a full range of health and social services across Alaska, California, Montana, New Mexico, Oregon, Texas and Washington. As a comprehensive health care organization, we are serving more people, advancing best practices and continuing our more than 100-year tradition of serving the poor and vulnerable. 
 The amounts listed are the base pay range; additional compensation may be available for this role, such as shift differentials, standby/on-call, overtime, premiums, extra shift incentives, or bonus opportunities. 
 Check out our benefits page for more information about our Benefits and Rewards. 
 Requsition ID: 199556 
 Company: Providence Jobs 
 Job Category: Development/Engineering 
 Job Function: Information Technology 
 Job Schedule: Full time 
 Job Shift: Day 
 Career Track: Leadership 
 Department: 4011 SS IS DEVICE MGMT 
 Address: WA Redmond 17425 NE Union Hill Rd 
 Work Location: Redmond Junction At Bear Creek 
 Pay Range: $86.19 - $155.74 
 The amounts listed are the base pay range; additional compensation may be available for this role, such as shift differentials, standby/on-call, overtime, premiums, extra shift incentives, or bonus opportunities. 
 Check out our benefits page for more information about our Benefits and Rewards. 
 Providence is proud to be an Equal Opportunity Employer. Providence does not discriminate on the basis of race, color, gender, disability, veteran, military status, religion, age, creed, national origin, sexual identity or expression, sexual orientation, marital status, genetic information, or any other basis prohibited by local, state, or federal law.

