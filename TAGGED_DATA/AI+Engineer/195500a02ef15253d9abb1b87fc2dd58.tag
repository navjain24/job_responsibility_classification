{
    "sentence": "Minimize/eliminate downtime associated with procuring, ingesting, and handling collection only after assets are no longer viable.",
    "TAG": "Minimize/eliminate downtime associated with procuring, ingesting, and handling collection only after assets are no longer viable."
}