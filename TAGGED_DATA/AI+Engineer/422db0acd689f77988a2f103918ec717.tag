{
    "sentence": "You will be responsible for defining the architecture of new features, implementation, and rollout, analyzing and accommodating dogfood and customer feedback.",
    "TAG": "Deliver, collect and label data that meet quality standards set by the feature team."
}