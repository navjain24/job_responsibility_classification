{
    "sentence": "Coordinate with data engineering teams to ensure the readiness and quality of data sets required for AI system development.",
    "TAG": "Deliver, collect and label data that meet quality standards set by the feature team."
}