{
    "sentence": "You will interact with Design & Product teams to help define requirements and will implement and measure customer metrics to track the health of your features.",
    "TAG": "Deliver, collect and label data that meet quality standards set by the feature team."
}