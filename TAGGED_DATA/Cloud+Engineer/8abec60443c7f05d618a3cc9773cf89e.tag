{
    "sentence": "8) Communicate progress proactively and regularly \u2013 using various tools and strategies (governance calls, stand-up calls, formal jeopardies, escalations, daily/weekly progress reports, defect metrics collections).",
    "TAG": "Manage security issues and engage with partner security teams and internal service teams to ensure timely remediation of issues, escalating as necessary to ensure appropriate levels of urgency and engagement."
}