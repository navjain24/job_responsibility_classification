{
    "sentence": "The Cloud Security team ensures the secure use of cloud services at Amazon, and balances our requirements against developer productivity and business needs.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}