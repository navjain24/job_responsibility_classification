{
    "sentence": "Work closely with Morgan Stanley and acquisition Cloud Security/Engineering staff to align security policies, IaC blueprints, and security guardrails.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}