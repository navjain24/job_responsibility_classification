{
    "sentence": "Collaborate within and across teams to design and build practical and extensible software solutions for future investment and growth.",
    "TAG": "Proactive knowledge-sharing within the team and across the company."
}