{
    "sentence": "Our primary focus is to ensure the security of our cloud products and continuously raise the security bar for our customers.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}