{
    "sentence": "actively seek areas of opportunity to improve the Security Incident Handling Lifecycle, saving time/effort while continuously delivering higher-quality outcomes for the security of AWS customers.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}