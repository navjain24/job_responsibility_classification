{
    "sentence": "You will work closely with cross-functional teams to ensure the scalability, reliability, and performance of our AWS-based systems.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}