{
    "sentence": "Ability to work well cross-functionally, and communicate with audiences who may not have a security background.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}