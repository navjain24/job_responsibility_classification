{
    "sentence": "Troubleshoot and resolve infrastructure-related issues and provide technical support to internal teams",
    "TAG": "Manage security issues and engage with partner security teams and internal service teams to ensure timely remediation of issues, escalating as necessary to ensure appropriate levels of urgency and engagement."
}