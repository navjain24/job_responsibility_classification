{
    "sentence": "This role requires Security Analysts to work with internal stakeholders to solve security challenges at massive scale, and to think strategically to develop and implement changes to drive automation, scalability, and continuous improvements across the organization.",
    "TAG": "Manage security issues and engage with partner security teams and internal service teams to ensure timely remediation of issues, escalating as necessary to ensure appropriate levels of urgency and engagement."
}