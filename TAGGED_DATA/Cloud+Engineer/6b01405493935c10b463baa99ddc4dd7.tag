{
    "sentence": "As an engineering team ourselves, we do this by building detection systems for major classes of security risk and coordinating with our policy, incident response, and platform teams.",
    "TAG": "Manage security issues and engage with partner security teams and internal service teams to ensure timely remediation of issues, escalating as necessary to ensure appropriate levels of urgency and engagement."
}