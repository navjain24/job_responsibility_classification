{
    "sentence": "We care about your career growth and strive to assign projects based on what will help each team member develop into a better-rounded security champion and enable them to take on more complex and higher-risk tasks in the future.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}