{
    "sentence": "Provide technical guidance and mentorship to junior team members, fostering a collaborative and knowledge-sharing culture within the organization.",
    "TAG": "Proactive knowledge-sharing within the team and across the company."
}