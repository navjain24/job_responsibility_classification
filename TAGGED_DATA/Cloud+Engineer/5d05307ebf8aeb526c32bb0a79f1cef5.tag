{
    "sentence": "be comfortable exemplifying the Amazonian Leadership Principle of \"Deliver Results\" and \"Bias for Action\", challenging decisions and escalating when appropriate.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}