{
    "sentence": "1+ years of experience developing software for or securing infrastructure running on AWS.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}