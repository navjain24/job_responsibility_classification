{
    "sentence": "As employees we come together with a growth mindset, innovate to empower others, and collaborate to realize our shared goals.",
    "TAG": "Proactive knowledge-sharing within the team and across the company."
}