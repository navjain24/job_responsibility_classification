{
    "sentence": "Demonstrate high capacity and tolerance for extreme context switching and interruptions while remaining productive and effective.",
    "TAG": "Demonstrate high capacity and tolerance for extreme context switching and interruptions while remaining productive and effective."
}