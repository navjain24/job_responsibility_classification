{
    "sentence": "Collaborate effectively with cross-functional teams, including development, operations, and security teams.",
    "TAG": "Manage security issues and engage with partner security teams and internal service teams to ensure timely remediation of issues, escalating as necessary to ensure appropriate levels of urgency and engagement."
}