{
    "sentence": "Identify, communicate, and collaborate with relevant stakeholders within one or more teams to drive impact and work toward mutual goals.",
    "TAG": "Proactive knowledge-sharing within the team and across the company."
}