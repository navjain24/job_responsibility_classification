{
    "sentence": "Join our innovative team and contribute to the ongoing success of our cloud infrastructure and services.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}