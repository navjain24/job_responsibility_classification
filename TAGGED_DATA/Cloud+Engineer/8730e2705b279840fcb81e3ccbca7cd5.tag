{
    "sentence": "Amazon\u2019s culture of inclusion is reinforced within our Leadership Principles, which remind team members to seek diverse perspectives, Learn and Be Curious, and Earn Trust.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}