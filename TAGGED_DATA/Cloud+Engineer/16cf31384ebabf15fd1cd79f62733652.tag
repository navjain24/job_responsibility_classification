{
    "sentence": "This role will provide career growth opportunities as you gain new security skills in the course of your duties.",
    "TAG": "Participate in efforts to promote security throughout the Company and build good working relationships within the team and with others across Amazon."
}